package com.athmanapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static android.support.v7.widget.AppCompatDrawableManager.get;

public class MainActivity extends AppCompatActivity {

    Button btn_screen2, btn_screen3, btn_screen4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        doOnClick();

    }

    public void doOnClick(){
        btn_screen2 = (Button)findViewById(R.id.btn_screen2);
        btn_screen3 = (Button)findViewById(R.id.btn_screen3);
        btn_screen4 = (Button)findViewById(R.id.btn_screen4);

        btn_screen2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Screen2.class);
                startActivity(i);

            }
        });

        btn_screen3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Screen3.class);
                startActivity(i);

            }
        });

        btn_screen4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Screen4.class);
                startActivity(i);

            }
        });
    }





}
